import note as nt
from options import *

import random
from operator import attrgetter

# Returns all the notes of the track that are playing at the time given
# TODO: harmonies should only occur on separate tracks, so this will have to get the chord accordingly
def getChord(track, time):
    notes = []
    for note in track:
        if note.delta <= time and note.end >= time:
            notes.append(note)
    notes.sort(key=lambda n: n.pitch)
    return notes

# Checks and ranks the interval distance between the notes played simultaneously
def rankHarmony(track, rank):
    for item in track:
        chord = getChord(track, item.delta)
        prev = item
        for note in chord:
            # Too high interval distance
            if abs(note.pitch - prev.pitch) > 12:
                rank -= abs(float(note.pitch - prev.pitch)) / 6
                prev = note
                continue # the chord will sound bad anyway, might as well earn time
            # Good = do mi sol, fa la do, sol si ré
            # Alt = ré fa la, la do mi
            # Bad = le reste
            pitch1 = note.pitch % 12
            pitch2 = prev.pitch % 12
            if note.pitch > prev.pitch:
                if pitch1 == nt.DO and pitch2 != nt.FA and pitch2 != nt.LA: # Do -> fa / la
                    rank -= 0.1
                elif pitch1 == nt.RE and pitch2 != nt.SOL and pitch2 != nt.SI: # Ré -> sol / si
                    rank -= 0.1
                elif pitch1 == nt.MI and pitch2 != nt.DO: # Mi -> do / alt la
                    rank -= 0.05
                    if pitch2 != nt.LA: # a La is not a crime however... maybe lower this
                        rank -= 0.05
                elif pitch1 == nt.FA and pitch2 != nt.RE: # Fa -> ré
                    rank -= 0.1
                elif pitch1 == nt.SOL and pitch2 != nt.DO and pitch2 != nt.MI: # Sol -> do / mi
                    rank -= 0.1
                elif pitch1 == nt.LA and pitch2 != nt.FA: # La -> fa / alt ré
                    rank -= 0.05
                    if pitch2 != nt.RE:
                        rank -= 0.05 # again, not a crime, maybe lower it
                elif pitch1 == nt.SI and pitch2 != nt.SOL: # Si -> sol
                    rank -= 0.1

            elif note.pitch < prev.pitch:
                if pitch1 == nt.DO and pitch2 != nt.MI and pitch2 != nt.SOL: # Do -> mi / sol
                    rank -= 0.1
                elif pitch1 == nt.RE and pitch2 != nt.FA and pitch2 != nt.LA: # Ré -> fa / la
                    rank -= 0.1
                elif pitch1 == nt.MI and pitch2 != nt.SOL: # Mi -> sol
                    rank -= 0.1
                elif pitch1 == nt.FA and pitch2 != nt.LA and pitch2 != nt.DO: # Fa -> la / do
                    rank -= 0.1
                elif pitch1 == nt.SOL and pitch2 != nt.SI and pitch2 != nt.RE: # Sol -> si / ré
                    rank -= 0.1
                elif pitch1 == nt.LA and pitch2 != nt.DO: # La -> do / alt mi
                    rank -= 0.05
                    if pitch2 != nt.MI:
                        rank -= 0.05 # again, not a crime, maybe lower it
                elif pitch1 == nt.SI and pitch2 != nt.RE: # Si -> ré
                    rank -= 0.1

            prev = note

    return rank

# Ranks the silences of the track: the higher the silence time, the greater
# the penalty.
def checkSilences(track, rank):
    covered = {}
    for note in track:
        try:
            if covered[note.delta]:
                if covered[note.delta] < note.dur:
                    covered[note.delta] = note.dur
        except:
            covered[note.delta] = note.dur
    # Count the silence time
    lastDelta = 0
    lastDur = 0
    genSilence = 0
    for delta in covered:
        # There has been a silence if the duration of the last note is inferior
        # to the difference between the last delta and this delta
        silence = (delta - lastDelta) - lastDur
        if silence > 0: # there was a silence
            genSilence += silence
        lastDelta = delta
        lastDur = covered[delta]

    rank -= genSilence * 5
    return rank

# Checks the intervals between each close note. If this interval is too high
# there is a high penalty.
def checkIntervals(track, rank):
    # First, order the track
    orderedTrack = sorted(track, key=attrgetter('delta'))

    # Then evaluate it
    prev = orderedTrack[0]
    for note in orderedTrack:
        interval = abs(note.pitch - prev.pitch)
        if interval > 12: # Too high interval
            rank -= float(interval) / 6
        elif interval not in {0, 2, 4, 7, 12}: # Best intervals
            if interval not in {5, 9, 11}: # OK intervals
                rank -= float(interval) / 6 # Bad interval
            else:
                rank -= float(interval) / 3 # OK interval
        prev = note
    return rank

# Ranks the current track. Each track has a base rank of 10, decreasing each
# time they break a rule.
def rankTrack(track):
    rank = 0
    rank = rankHarmony(track, rank)
    rank = checkIntervals(track, rank)
    rank = checkSilences(track, rank)
    # Divide the rank by the number of notes in the track to equalize the tracks
    rank = rank / len(track)
    return rank

# Ranks the tracks and selects the best tracks to pick from
def selectTracks(tracks, allTimeBest):
    ranks = []
    for track in tracks:
        ranks.append(rankTrack(track))
    mx = abs(min(ranks)) + 1
    for i in range(0, len(ranks)):
        ranks[i] += mx

    # allTimeBest = elitism, stores the X all time best tracks and carry them
    # over in each generation
    if len(allTimeBest) == 0: # initialize allTimeBest
        for i in range(0, len(ranks)):
            allTimeBest.append([tracks[i], ranks[i]])
            if len(allTimeBest) == GENSIZE / ELITEFREQ:
                allTimeBest.sort(key=lambda x: x[1]) # sort by rank
                break
    # Update the list of all time best tracks
    for i in range(0, len(ranks)):
        if ranks[i] > allTimeBest[0][1]: # Best rank -> replace it
            allTimeBest[0][1] = ranks[i]
            allTimeBest[0][0] = tracks[i]
            allTimeBest.sort(key=lambda x: x[1]) # sort by rank

    # Turn the ranking of the track into a rank number
    ladder = sorted(ranks)
    for i in range(0, len(ranks)):
        ranks[i] = ladder.index(ranks[i]) + 1

    # Rank-based selection
    bestTracksNo = []
    for i in range(0, GENSIZE - int(GENSIZE / ELITEFREQ)):
        ar = [x for x in range(0, GENSIZE)]
        candidate = random.choices(ar, ranks)[0]
        if i > 4:
            while candidate == bestTracksNo[i - 5]:
                candidate = random.choices(ar, ranks)[0]
        bestTracksNo.append(candidate)

    # Store selected tracks in bestTracks and return them
    bestTracks = []
    for i in bestTracksNo:
        bestTracks.append(tracks[i])
    return bestTracks, allTimeBest
