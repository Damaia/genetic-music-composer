CHUNKDUR = 8 # Duration of a chunk of music
DURATION = 12 # Initial duration of the track

ENDMUTATION = 0.2 # Probability that the block mutations are added at the end of the track instead of right after the copied block
MUTFREQ = 0.7

GENERATIONS = 200 # Number of generations
GENSIZE = 100 # Number of tracks in one generation
ELITEFREQ = 50 # 1 track in ELITEFREQ will be put in the elite array
DOCROSSOVER = True

OUTPUT_DEBUG = False
