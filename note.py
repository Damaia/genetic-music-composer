class Note(object):
    delta = 0.0
    pitch = 60
    dur = 1.0
    end = 1.0

    def __init__(self, delta, pitch = 60, dur = 1.0):
        super(Note, self).__init__()
        self.delta = delta
        self.pitch = pitch
        self.dur = dur

    def __repr__(self):
        return str(self.delta) + ' to ' + str(self.delta + self.dur) + ': ' + str(self.pitch) + ' ' + str(self.dur) + '\n'

# The aim is to output C Major so we keep the pitches of the range somewhere
# in order to get them back easily
CMajorRange = [0, 2, 4, 5, 7, 9, 11]
DO = 0
RE = 2
MI = 4
FA = 5
SOL = 7
LA = 9
SI = 11
