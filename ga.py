import midi
import note as nt
import mutations
import evaluation
from copy import deepcopy
from options import *

import random
import datetime
import os

mutationList = [mutations.inversion, mutations.transposition, mutations.elongation, mutations.pitchMutation, mutations.splitMutation]

# NOTE: Should we remove the crossover ?

def initTrack():
    track = []
    length = 0
    while length < DURATION:
        pitch = random.choices(nt.CMajorRange, [2, 1, 2, 1, 2, 1, 1])[0]
        pitchModifier = random.choices([48, 60, 72], [1, 10, 1])[0]
        pitch = pitch + pitchModifier
        # Weights are put here so that shorter durations happen more frequently
        dur = float(random.choices([1, 2, 3, 4, 5, 6, 7, 8], [32, 16, 1, 4, 1, 1, 1, 2])[0]) / 2
        if length + dur > DURATION:
            dur = DURATION - length
        track.append(nt.Note(length, pitch, dur))
#        delta = random.random()
#        if delta <= 0.9: # Generated note can be a chord
        length += dur
#        elif delta <= 0.9:
#            length += dur / 2
    return track

def mutateTrack(track):
    if random.random() > MUTFREQ:
        mutation = random.choice(mutationList)
        track = mutation(track)
    return track

def evolveGen(tracks, allTimeBest):
    # First, mix tracks with allTimeBest
    for t in allTimeBest:
        tracks.append(t[0])
    newGen = []
    for i in range(0, int(len(tracks) / 2)):
        # Step 1: select the tracks
        # The tracks have already been pre-selected by the evaluation function.
        track1 = tracks[i]
        track2 = tracks[i + int(len(tracks) / 2)]

        # Compute the length of each track
        l1 = 0
        for note in track1:
            if note.delta + note.dur > l1:
                l1 = note.delta + note.dur
        l2 = 0
        for note in track2:
            if note.delta + note.dur > l2:
                l2 = note.delta + note.dur
        dur = l1
        if l1 > l2:
            dur = l2

        newTrack1 = []
        newTrack2 = []
        if DOCROSSOVER:
            # Step 2: crossover between the two tracks
            # Chose the crossover point at random
            crossover = random.randrange(1, round(dur) - 1)
#            print(track1)
#            print(track2)
#            print('Crossover delta: ' + str(crossover))
            # Now generate the two new elements
            for note in track1:
                if note.delta < crossover:
                    dur = note.dur
                    if note.delta + note.dur > crossover: # will overlap -> reduce length
                        dur = crossover - note.delta
                        # Add the remainer of the note to the other track, as the first note after the crossover point
                        newTrack2.append(nt.Note(crossover, note.pitch, note.dur - dur))
                    newTrack1.append(nt.Note(note.delta, note.pitch, dur))
                else:
                    newTrack2.append(nt.Note(note.delta, note.pitch, note.dur))
            for note in track2:
                if note.delta < crossover:
                    dur = note.dur
                    if note.delta + note.dur > crossover: # will overlap -> reduce length
                        dur = crossover - note.delta
                        newTrack1.append(nt.Note(crossover, note.pitch, note.dur - dur))
                    newTrack2.append(nt.Note(note.delta, note.pitch, dur))
                else:
                    newTrack1.append(nt.Note(note.delta, note.pitch, note.dur))
#            print('===>')
#            print(newTrack1)
#            print(newTrack2)
#            print('')
#            print('----------')
#            print('')
        else:
            for note in track1:
                newTrack1.append(nt.Note(note.delta, note.pitch, note.dur))
            for note in track2:
                newTrack2.append(nt.Note(note.delta, note.pitch, note.dur))

        # Step 3: mutations
        newTrack1 = mutateTrack(newTrack1)
        newTrack2 = mutateTrack(newTrack2)

        # Step 4: add the new tracks to the new generation
        newGen.append(newTrack1)
        newGen.append(newTrack2)

    return newGen

def main():
    random.seed()
    seed = random.randrange(1000000)
    random.seed(seed)
    date = str(datetime.datetime.now())
    folder = 'tracks/' + date[:-7] + '_Seed' + str(seed)
    # Create output directory
    if not os.path.exists(folder):
        os.makedirs(folder)

    tracks = []
    gens = []
    allTimeBest = []
    for i in range(0, GENSIZE):
        tracks.append(initTrack())
    gens.append(tracks) # store first generation
    for i in range(0, GENERATIONS):
        trackPool, allTimeBest = evaluation.selectTracks(gens[i], allTimeBest)
        gens.append(evolveGen(trackPool, allTimeBest))
        if i % 50 == 0:
            print('Gen ' + str(i))
            for j in range(0, GENSIZE):
                midi.generateFile(folder + '/Gen' + str(i) + '_' + str(j), gens[i][j])
    notes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] # array of encountered notes for stats
    for j in range(0, GENSIZE):
        for note in gens[-1][j]:
            notes[note.pitch % 12] += 1 # increase the number of the encountered note (stats)
        midi.generateFile(folder + '/LastGen_' + str(j), gens[-1][j])
    for j in range(0, len(allTimeBest)):
        midi.generateFile(folder + '/AllTimeBest_' + str(j), allTimeBest[j][0])
    for i in range(0, len(notes)):
        print('Number of ' + str(i) + ' : ' + str(notes[i])) # print stats

main()
