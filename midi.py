from midiutil.MidiFile import *

import note
from options import *

def generateFile(out, track):
    # Output the notes in a separate file for debug
    if OUTPUT_DEBUG:
        track = sorted(track, key=lambda note: note.delta)
        output_file = open(out + '.txt', 'w')
        for note in track:
            output_file.write(str(note))
        output_file.close()

    # Create MIDI object
    mf = MIDIFile(2, True, False, True, 1) # number of tracks, removeDuplicates, deinterleave, adujst_origin, file_format
    mf.addTimeSignature(0, 0, 4, 2, 24, 8)
    mf.addKeySignature(0, 0, 0, SHARPS, MAJOR)
    mf.addTrackName(0, 0, 'Test track')

    # Add notes to MIDI object
    for note in track:
        mf.addNote(0, 0, note.pitch, note.delta, note.dur, 100)

    # Write to file
    with open(out + '.mid', 'wb') as output_file:
        mf.writeFile(output_file)
