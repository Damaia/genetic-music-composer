import note as nt
from options import *

import random

# TODO: mutations that affect one sequence (about two steps):
# retrograde (left/right), retrograde+inversion...
# TODO: for chunk mutations, the firt note should be aligned with the note
# right before

def getTheme(track):
    # The theme is the ensemble of the notes between delta 0 and delta 8.
    theme = []
    for note in track:
        if note.delta <= 8:
            # Copy the note, not the pointer
            theme.append(nt.Note(note.delta, note.pitch, note.dur))
    return theme

# Returns the duration of the track, ie the last delta + the last dur
def getDuration(track):
    delta = 0
    for note in track:
        if note.delta + note.dur > delta:
            delta = note.delta + note.dur
    return delta

# Returns a random chunk of the track
# Take an arbitrary number of steps or an arbitrary number of notes?
def getChunk(track):
    chunk = []
    begin = 0
    lastDelta = 0
    dur = getDuration(track)
    if int(getDuration(track)) - CHUNKDUR > 0:
        begin = random.randrange(int(getDuration(track)) - CHUNKDUR)
    for note in track:
        if note.delta >= begin and note.delta + note.dur < begin + CHUNKDUR:
            chunk.append(nt.Note(note.delta, note.pitch, note.dur))
            if note.delta + note.dur > lastDelta:
                lastDelta = note.delta + note.dur
    # Align "begin" with the actual first note, in case there is no note
    # at this point
    if len(chunk) > 0:
        begin = chunk[0].delta
        for note in chunk:
            if note.delta < begin:
                begin = note.delta
    return chunk, begin, lastDelta

# Repeat the theme, with or without moduling it first, and add it to the end of
# the track
def repeatTheme(track):
    theme = getTheme(track)
    delta = getDuration(track)
    newTonality = random.choice([-4, -2, 0, 2, 4])
    for note in theme:
        note.pitch += newTonality
        note.delta += delta
        if note.pitch < 0 or note.pitch > 255: # Would break the track -> cancel mutation
            return track
    track.extend(theme)
    return track

def inversion(track):
    # Get a random chunk of the track
    chunk, begin, end = getChunk(track)
    if len(chunk) == 0:
        return track

    # 'Choose' whether to add the mutation at the end of the track or right
    # after the copied block
    if random.random() < ENDMUTATION:
        addEnd = True
        dur = getDuration(track) - begin + 1 # length of the track - offset of the first note
    else:
        addEnd = False
        dur = end - begin

    # Order the chunk's notes
    chunk = sorted(chunk, key=lambda note: note.delta)
#    print(chunk)

    # Reverse the tonalities
    firstPitch = chunk[0].pitch
    for note in chunk:
        note.delta += dur
        diff = firstPitch - note.pitch
        if note.pitch + diff * 2 > 255 or note.pitch + diff * 2 < 0:
            return track
        note.pitch += diff * 2

    # If the mutation block is added right after the copied chunk,
    # reorder the track's notes so that there is no collision
#    print('===>')
#    print(chunk)
#    print('begin: ' + str(begin) + '  end: ' + str(end) + ' ==> dur: ' + str(dur))
    if not addEnd:
        track = sorted(track, key=lambda note: note.delta)
        for note in track:
            if note.delta >= end:
#                print('note has delta ' + str(note.delta) + ', changing it to ' + str(note.delta + dur))
                note.delta += dur
            else:
                pass
#                print('note has delta ' + str(note.delta) + ', no change to be done')

    # Add it to the track
    track.extend(chunk)
    return track

# Repeat a chunk of the track
def transposition(track):
    # Get a random chunk of the track
    chunk, begin, end = getChunk(track)
    if len(chunk) == 0:
        return track

    # 'Choose' whether to add the mutation at the end of the track or right
    # after the copied block
    if random.random() < ENDMUTATION:
        addEnd = True
        dur = getDuration(track) - begin + 1 # length of the track - offset of the first note
    else:
        addEnd = False
        dur = end - begin

    # Modify the tonality of the chunk
    newTonality = random.choice([-4, -2, 0, 2, 4])
    for note in chunk:
        note.pitch += newTonality
        note.delta += dur
        if note.pitch < 0 or note.pitch > 255: # Would break the track -> cancel mutation
            return track

    # If the mutation block is added right after the copied chunk,
    # reorder the track's notes so that there is no collision
    if not addEnd:
        for note in track:
            if note.delta >= end:
                note.delta += dur

    # Add it to the track
    track.extend(chunk)
    return track

# Repeat a chunk of the track, but two times slower
def elongation(track):
    # Get a random chunk from the track
    chunk, begin, end = getChunk(track)
    if len(chunk) == 0:
        return track
    chunk2 = []

    # Order the chunk's notes
    chunk = sorted(chunk, key=lambda note: note.delta)

    # 'Choose' whether to add the mutation at the end of the track or right
    # after the copied block
    if random.random() < ENDMUTATION:
        addEnd = True
        dur = getDuration(track) - begin + 1 # length of the track - offset of the first note
    else:
        addEnd = False
        dur = end - begin

    # Double the duration of the notes of the chunk
    for note in chunk:
        note.delta += dur
        dur += note.dur
        note.dur *= 2

    # If the mutation block is added right after the copied chunk,
    # reorder the track's notes so that there is no collision
    if not addEnd:
        for note in track:
            if note.delta >= end:
                note.delta += dur

    # Add it to the track
    track.extend(chunk)
    track.extend(chunk2)
    return track

# BUG: All the notes should be reordered here!
def durationMutation(track):
    note = random.randrange(len(track))
    durChange = random.randrange(10) # higher chance of having a longer note
    if durChange < 7 or track[note].dur <= 0.25:
        track[note].dur *= 2
    else:
        track[note].dur /= 2
    return track

def splitMutation(track):
    # Split one note into two notes of half its duration
    noteID = random.randrange(len(track))
    note = track.pop(noteID)
    if note.dur <= 0.25: # Don't split, it's already very short
        track.insert(noteID, note)
        return track
    childNote1 = nt.Note(note.delta, note.pitch, note.dur / 2)
    childNote2 = nt.Note(note.delta + note.dur / 2, note.pitch, note.dur / 2)
    track.insert(noteID, childNote1)
    track.insert(noteID, childNote2)
    return track

def acceptablePitchChange(newPitch, pitch):
    if newPitch - pitch > 12 or newPitch - pitch < -12:
        return False
    if newPitch % 12 not in nt.CMajorRange:
        return False
    return True

def pitchMutation(track):
    # Change the pitch of one note.
    # Weights should be put to decide to which note it will change.
    # If the new note is in the major chord it should have a greater weight
    # If the new note is only one tone below or above it should have a medium
    # weight
    # Else the change is probably bad.
    # Also, the new note should not be far away from the initial note (less
    # than 8 tones or 12 semitones)
    note = random.randrange(len(track))
    newPitch = random.randrange(127)
    while not acceptablePitchChange(newPitch, track[note].pitch):
        newPitch = random.randrange(127)
    track[note].pitch = newPitch
    return track
